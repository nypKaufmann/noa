package ch.nyp.noa.config.generic;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import ch.nyp.noa.webContext.domain.authority.Authority;
import ch.nyp.noa.webContext.domain.role.Role;
import ch.nyp.noa.webContext.domain.user.User;
import io.swagger.annotations.ApiModel;

/**
 * Class that covers the common attributes of all entities
 *
 * @author Yves Kaufmann
 */
@MappedSuperclass
@ApiModel(value = "ExtendedEntity", discriminator = "SuperClass", subTypes = { Authority.class, Role.class,
		User.class })
//@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "ExtendedEntity", visible = true)
//@JsonSubTypes({ @Type(value = User.class),@Type(value = Role.class),@Type(value = Authority.class) })
public abstract class ExtendedEntity {

	// IDENTITY vs SEQUENCE
	// https://www.thoughts-on-java.org/hibernate-postgresql-5-things-need-know/
	// @GeneratedValue(strategy = GenerationType.SEQUENCE, generator =
	// "user_generator")
	// @SequenceGenerator(name="user_generator", sequenceName = "user_seq_id")

	@Id
	@JsonProperty("id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private long id;
	
	/**
	 * 
	 */
	public ExtendedEntity() {
		super();
	}

	/**
	 * @param id
	 */
	public ExtendedEntity(long id) {
		super();
		this.id = id;
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

}