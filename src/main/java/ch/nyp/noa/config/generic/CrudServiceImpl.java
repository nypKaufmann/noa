package ch.nyp.noa.config.generic;

import java.util.List;
import java.util.Optional;

/**
 * Generic abstract class that implements the CrudService
 *
 * @author Yves Kaufmann
 */
public abstract class CrudServiceImpl<T extends ExtendedEntity> implements
		CrudService<T> {
	
	protected ExtendedJpaRepository<T> repository;

	/**
	 * @param repository
	 */
	public CrudServiceImpl(ExtendedJpaRepository<T> repository) {
		this.repository = repository;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void save(T entity) {
		repository.save(entity);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void delete(T entity) {
		repository.delete(entity);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deleteById(Long id) {
		repository.deleteById(id);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void update(T entity) {
		repository.save(entity);
		
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<T> findAll() {
		return repository.findAll();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public T findById(Long id) {
		Optional<T> entity = repository.findById(id);
		return entity.get();
	}
	
}
