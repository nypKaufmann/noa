package ch.nyp.noa.webContext.domain.user;

import javax.validation.ValidationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**
 * Rest Controller Principal (User) Responsible for the user that is currently
 * authenticated and logged in
 *
 * @author Yves Kaufmann
 */
@RestController
@RequestMapping("/principal")
public class PrincipalController {

	private UserServiceImpl userServiceImpl;

	/**
	 * 
	 */
	public PrincipalController() {
		super();
	}

	/**
	 * @param userServiceImpl
	 * @param userValidator
	 */

	@Autowired
	public PrincipalController(UserServiceImpl userServiceImpl) {
		super();
		this.userServiceImpl = userServiceImpl;
	}

	/**
	 * Gets currently logged in User
	 * 
	 * @return ResponseEntity with the User that is currently logged in
	 */
	@GetMapping("/getPrincipal")
	@ApiOperation(value = "Returns currently logged in User", notes = "Lorem Ipsum Kipsum Blub", response = User.class)
	public ResponseEntity<User> getPrincipal() {
		UserDetailsImpl userDetails = (UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication()
				.getPrincipal();
		User user = userDetails.getUser();
		return new ResponseEntity<User>(user, HttpStatus.OK);
	}

	/**
	 * @PostMapping("/createPrincipal/{id}") public User
	 * createPrincipal(@RequestBody UserDTO userDTO) {return null;}
	 */

	/**
	 * Updates the User that is currently logged in
	 * 
	 * @param user User that the currently logged in user should get updated to
	 * @return ResponseEntity with the updated user
	 */
	@PutMapping("/updatePrincipal")
	public ResponseEntity<User> updatePrincipal(@RequestBody User user) {
		userServiceImpl.save(user);
		// TO DO userServiceImpl.refresh(user);
		return new ResponseEntity<User>(user, HttpStatus.OK);
	}

	/**
	 * Gets currently logged in user
	 * 
	 * @return ResponseEntity with the outcome of the deletion process
	 */
	@DeleteMapping("/deletePrincipal")
	public ResponseEntity<Void> deletePrincipal() {
		UserDetailsImpl userDetails = (UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication()
				.getPrincipal();
		User user = userDetails.getUser();
		userServiceImpl.deleteById(user.getId());
		return new ResponseEntity<Void>(HttpStatus.OK);
	}
}
