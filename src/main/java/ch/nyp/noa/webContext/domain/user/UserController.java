package ch.nyp.noa.webContext.domain.user;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import ch.nyp.noa.webContext.domain.user.dto.UserDTO;


@RestController
public class UserController {

	// Services
	private UserServiceImpl userServiceImpl;
	
	private UserDTOValidator userValidator;

	/**
	 * 
	 */
	public UserController() {
	}

	/**
	 * @param userServiceImpl
	 */
	@Autowired
	public UserController(UserServiceImpl userServiceImpl, UserDTOValidator userValidator) {
		this.userServiceImpl = userServiceImpl;
		this.userValidator = userValidator;
	}
	
	@InitBinder
	public void dataBinding(WebDataBinder binder) {
		binder.addValidators(userValidator);
	}

	/**
	 * Gets a User with the given id
	 * 
	 * @param id Id of the requested User
	 * @return ResponseEntity with the User that was requested
	 */
	@GetMapping("/getUser/{id}")
	public ResponseEntity<UserDTO> getUser(@PathVariable("id") Long id) {
		// return new ResponseEntity<User>(user, HttpStatus.OK);
		return null;
	}

	/**
	 * Creates a User with the given user entity
	 * 
	 * @return ResponseEntity with the User that was created
	 */
	@PostMapping("/createUser")
	public ResponseEntity<UserDTO> createUser(@Valid @RequestBody UserDTO userDTO) {
		
		return new ResponseEntity<UserDTO>(userDTO, HttpStatus.OK);
	}

	/**
	 * Updates a User with the given user entity
	 * 
	 * @param id   Id of the User that should get updated
	 * @param user User entity to which the requested user should get updated to
	 * @return ResponseEntity with the updated User
	 */
	@PutMapping("/updatePrincipal/{id}")
	public ResponseEntity<UserDTO> updateUser(@PathVariable("id") Long id, @RequestBody User user) {
		// return new ResponseEntity<User>(user, HttpStatus.OK);
		return null;
	}

	/**
	 * Deletes a user
	 * 
	 * @return ResponseEntity with the outcome of the deletion process
	 */
	@DeleteMapping("/deleteUser/{id}")
	public ResponseEntity<UserDTO> deleteUser(@PathVariable("id") Long id) {
		// return new ResponseEntity<Void>(HttpStatus.OK);
		return null;
	}

}
