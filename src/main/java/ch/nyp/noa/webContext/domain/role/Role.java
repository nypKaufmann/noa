package ch.nyp.noa.webContext.domain.role;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import ch.nyp.noa.config.generic.ExtendedEntity;
import ch.nyp.noa.webContext.domain.authority.Authority;
import io.swagger.annotations.ApiModel;

import java.util.HashSet;
import java.util.Set;

/**
 * Entity Role. A user can hold multiple roles e.g "ROLE_ADMIN"
 * "ROLE_ACENTUATOR"
 *
 * @author Yves Kaufmann
 */
@Entity
@Table(name = "role")
@ApiModel(value="Role", description="A User can hold multiple Roles, e.g ROLE_ADMIN")
public class Role extends ExtendedEntity {

	@Column(name = "name")
	private String name;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "role_authority", joinColumns = @JoinColumn(name = "role_id"), inverseJoinColumns = @JoinColumn(name = "authority_id"))
	private Set<Authority> authorities = new HashSet<>();

	/**
	 * 
	 */
	public Role(Long id) {
		super(id);
	}

	/**
	 * @param id
	 * @param role
	 * @param authorities
	 */
	public Role(Long id, String name, Set authorities) {
		super(id);
		this.name = name;
		this.authorities = authorities;
	}

	/**
	 * @return the role
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param role the role to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the authorities
	 */
	public Set<Authority> getAuthorities() {
		return authorities;
	}

	/**
	 * @param authorities the authorities to set
	 */
	public void setAuthorities(Set<Authority> authorities) {
		this.authorities = authorities;
	}
	
}
