package ch.nyp.noa.webContext.domain.authority;

import ch.nyp.noa.config.generic.CrudService;

/**
* Service Authority  
*
* @author Yves Kaufmann
*/
interface AuthorityService extends CrudService<Authority>{
	
	/**
	 * Finds an Authority with a given name
	 *
	 * @param name Descriptive name of Authority
	 * @return Returns requested Authority with given descriptive name of Authority
	 */
	Authority findByName(String name);

	/**
	 * Deletes the requested authority with a given name
	 *
	 * @param name Descriptive name of Authority
	 */
	void deleteByName(String name);

}
