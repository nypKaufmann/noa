package ch.nyp.noa.webContext.domain.user;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import ch.nyp.noa.config.generic.ExtendedValidator;
import ch.nyp.noa.webContext.domain.user.dto.UserDTO;
import ch.nyp.noa.webContext.validation.ValidationAssistance;

@Component
public class UserDTOValidator implements Validator{

	private ValidationAssistance validationAssistance;

	/**
	 * 
	 */
	public UserDTOValidator() {

	}

	/**
	 * @param userService
	 */
	public UserDTOValidator(ValidationAssistance validationAssistance) {
		this.validationAssistance = validationAssistance;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean supports(Class<?> clazz) {
		return UserDTO.class.isAssignableFrom(clazz);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void validate(Object target, Errors errors) {
		
		UserDTO userDTO = (UserDTO) target;
		
		validationAssistance.evaluateIfEmptyOrWhitespace(errors, "username");
		
		
		if (this.isEmptyOrWhitespace(userDTO.getUsername())) {
			errors.rejectValue("username", "4001", "");
		}
		
		
		
		; 
		
		
		 if (user.getEmailAddress() != null && !EMAIL_REGEX.matcher(user.getEmailAddress()).matches()) {
	          errors.rejectValue("emailAddress", "user.email.invalid");
	      }
		
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "username", "4001", "Firstname is required.");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "4001",  "Firstname is required.");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "firstname", "4001", "LastName is required.");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "lastname", "4001", "LastName is required.");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "4001", "UserName is required.");

		
		
		
	
		if (userDTO.getId() < 1) {
			errors.rejectValue("id", "4001", "Id <= 0");
		} else if (userService.findById(userDTO.getId()) == null) {
			errors.rejectValue("id", "4002", "No user exists with id " + userDTO.getId());
		}

		// Role Validator , Authority Validator

		// ValidationUtils.invokeValidator(this.emailValidator, userDTO.getEmail(),
		// errors);

	}
}
