package ch.nyp.noa.webContext.domain.role;

import ch.nyp.noa.config.generic.CrudService;

/**
* Service Role  
*
* @author Yves Kaufmann
*/
interface RoleService extends CrudService<Role>{

	/**
	 * Finds an role with a given name
	 *
	 * @param name Descriptive name of role
	 * @return Returns requested role with given descriptive name of role
	 */
	Role findByName(String name);

	/**
	 * Deletes the requested role with a given name
	 *
	 * @param name Descriptive name of role
	 */
	void deleteByName(String name);

}
