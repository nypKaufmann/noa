package ch.nyp.noa.webContext.domain.authority;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ch.nyp.noa.config.generic.CrudServiceImpl;

/**
* Service implementation Authority
*
* @author Yves Kaufmann
*/
@Service
class AuthorityServiceImpl extends CrudServiceImpl<Authority> implements
AuthorityService {

	/**
	 * @param repository
	 */
	@Autowired
	AuthorityServiceImpl(AuthorityRepository repository) {
		super(repository);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deleteByName(String name) {
		
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Authority findByName(String name) {
		// TODO Auto-generated method stub
		return null;
	}

}
