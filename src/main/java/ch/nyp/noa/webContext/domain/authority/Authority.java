package ch.nyp.noa.webContext.domain.authority;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import ch.nyp.noa.config.generic.ExtendedEntity;
import io.swagger.annotations.ApiModel;

/**
 * Entity Authority. A role can hold multiple authorities e.g "WRITE_PRIVILEDGE" or "READ_PRIVILEDGE" 
 *
 * @author Yves Kaufmann
 */
@Entity
@Table(name = "authority")
@ApiModel(value="Authority", description="A Role can hold multiple Authorities, e.g WRITE_PRIVILEDGE or READ_PRIVILEDGE")
public class Authority extends ExtendedEntity{

	@Column(name = "name")
	private String name;

	/**
	 * 
	 */
	public Authority(Long id) {
		super(id);
	}

	/**
	 * @param id
	 * @param authority
	 */
	public Authority(int id, String name) {
		super(id);
		this.name = name;
	}

	/**
	 * @return the authority
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param authority
	 *            the authority to set
	 */
	public void setName(String name) {
		this.name = name;
	}

}
