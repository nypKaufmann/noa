package ch.nyp.noa.webContext.domain.role;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ch.nyp.noa.config.generic.CrudServiceImpl;

/**
* Service implementation Role
*
* @author Yves Kaufmann
*/
@Service
class RoleServiceImpl extends CrudServiceImpl<Role> implements RoleService{

	/**
	 * @param repository
	 */
	@Autowired
	RoleServiceImpl(RoleRepository repository) {
		super(repository);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Role findByName(String name) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deleteByName(String name) {
		// TODO Auto-generated method stub
		
	}
	
}
