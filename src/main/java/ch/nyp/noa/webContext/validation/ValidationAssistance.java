package ch.nyp.noa.webContext.validation;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;

import ch.nyp.noa.config.PropertyReader;

public class ValidationAssistance extends ValidationUtils {

	private PropertyReader propertyReader;

	private static final Pattern USERNAME_REGEX = Pattern.compile("");
	private static final Pattern PASSWORD_REGEX = Pattern.compile("");
	private static final Pattern EMAIL_REGEX = Pattern.compile("");
	private static final Pattern DATE_REGEX = Pattern.compile("");

	public ValidationAssistance() throws IOException {
		this.propertyReader = new PropertyReader("messaged.properties");
	}

	public boolean evaluateIfEmptyOrWhitespace(Errors error, String field) {
		Object value = error.getFieldValue(field);
		if (value == null || !StringUtils.hasText(value.toString())) {
			error.rejectValue(field, "4001", propertyReader.getStringProperty("4001"));
			return true;
		}
		return false;
	}

	public boolean evaluateIfEmpty(Errors error, String field) {
		Object value = error.getFieldValue(field);
		if (value == null || !StringUtils.hasLength(value.toString())) {
			error.rejectValue(field, "4002", propertyReader.getStringProperty("4002"));
			return true;
		}
		return false;
	}

	public boolean evaluateIfEmailValid(Errors error, String field) {
		String value = (error.getFieldValue(field)).toString();
		Matcher m = EMAIL_REGEX.matcher(value);
		if (!m.matches()) {
			error.rejectValue(field, "4003", propertyReader.getStringProperty("4003"));
			return true;
		}
		return false;
	}
}
